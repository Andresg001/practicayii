<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tipo_proveedor".
 *
 * @property int $id_proveedor
 * @property string $nombre
 * @property string $observacion
 *
 * @property TbProveedores[] $tbProveedores
 */
class Tipoproveedor extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tipo_proveedor';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'observacion'], 'required'],
            [['nombre', 'observacion'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_proveedor' => 'Id Proveedor',
            'nombre' => 'Nombre',
            'observacion' => 'Observacion',
        ];
    }

    /**
     * Gets query for [[TbProveedores]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTbProveedores()
    {
        return $this->hasMany(TbProveedor::className(), ['tipo_provedores' => 'id_proveedor']);
    }
}
