<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tb_proveedores".
 *
 * @property int $id
 * @property string $ruc
 * @property string $nombre_comercial
 * @property int $tipo_provedores
 * @property string $direccion
 * @property string $telefono
 * @property string $email
 * @property string $fecha_envio
 *
 * @property TipoProveedor $tipoProvedores
 */
class Tbproveedor extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tb_proveedores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ruc', 'nombre_comercial', 'tipo_provedores', 'fecha_envio'], 'required'],
            [['ruc'],'unique', 'message'=>'Éste Ruc ya es existente'],
            [['tipo_provedores'], 'integer'],
            [['ruc'], 'string', 'max' => 120],
            [['nombre_comercial', 'direccion', 'telefono', 'email'], 'string', 'max' => 250],
            [['fecha_envio'], 'string', 'max' => 150],
            [['tipo_provedores'], 'exist', 'skipOnError' => true, 'targetClass' => TipoProveedor::className(), 'targetAttribute' => ['tipo_provedores' => 'id_proveedor']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ruc' => 'Ruc',
            'nombre_comercial' => 'Nombre Comercial',
            'tipo_provedores' => 'Tipo Provedores',
            'direccion' => 'Dirección',
            'telefono' => 'Telefono',
            'email' => 'Email',
            'fecha_envio' => 'Fecha de convenio',
        ];
    }

    /**
     * Gets query for [[TipoProvedores]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTipoProvedores()
    {
        return $this->hasOne(TipoProveedor::className(), ['id_proveedor' => 'tipo_provedores']);
    }
}
