<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Tbproveedor */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbproveedor-form">

    <?php $form = ActiveForm::begin(); ?>


    <?= $form->field($model, 'ruc')->textInput(['class'=>'form-control','placeholder'=>'Ruc']); ?>

    <?= $form->field($model, 'nombre_comercial')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tipo_provedores')->widget(Select2::class, [
                        'data' => $listaProveedores,
                        'options' => ['placeholder' => Yii::t('app', 'Seleccione un proveedor ') . '...'],
                        'pluginOptions' => [
                            'allowClear' => true,
                        ],
                    ])
   ?> 

    <?= $form->field($model, 'direccion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'telefono')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fecha_envio')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
