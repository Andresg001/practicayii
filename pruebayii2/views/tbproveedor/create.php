<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Tbproveedor */

$this->title = 'Create Tbproveedor';
$this->params['breadcrumbs'][] = ['label' => 'Tbproveedors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbproveedor-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'listaProveedores' => $listaProveedores
    ]) ?>

</div>
